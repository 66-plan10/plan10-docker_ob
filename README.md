# Docker Base Image for Obarun
This repository contains all scripts and files needed to create a Docker base image for the Obarun build concept.
## Dependencies
Install the following packages:
* make
* archlinux-install-scripts
* docker
* zsh
## Usage
Run `make image` as root to build the base image.
## Purpose
* Provide the Obarun experience in a Docker Image
* Provide the most simple but complete image to base every other upon
* `pacman` needs to work out of the box
* All installed packages have to be kept unmodified
