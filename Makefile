BUILDDIR=$(shell pwd)/build
OUTPUTDIR=$(shell pwd)/output
DOCKER_USER:=obarun
DOCKER_ORGANIZATION=obarun
DOCKER_IMAGE:=base

define rootfs
	mkdir -vp $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks
	find /usr/share/libalpm/hooks -exec ln -sf /dev/null $(BUILDDIR)/alpm-hooks{} \;

	mkdir -vp $(BUILDDIR)/var/lib/pacman/ $(OUTPUTDIR)
	install -Dm644 pacman.conf $(BUILDDIR)/etc/pacman.conf
	cat pacman-conf.d-noextract.conf >> $(BUILDDIR)/etc/pacman.conf

	fakechroot -- fakeroot -- pacman -Sy -r $(BUILDDIR) \
		--noconfirm --dbpath $(BUILDDIR)/var/lib/pacman \
		--config $(BUILDDIR)/etc/pacman.conf \
		--noscriptlet \
		--hookdir $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks/ $(shell cat packages)

	cp --recursive --preserve=timestamps --backup --suffix=.pacnew rootfs/* $(BUILDDIR)/

	fakechroot -- fakeroot -- chroot $(BUILDDIR) update-ca-trust
	fakechroot -- fakeroot -- chroot $(BUILDDIR) bash -c \
		"locale-gen ; \
		pacman-key --init ; \
		pacman-key --populate archlinux obarun ;"

	fakechroot -- fakeroot -- chroot $(BUILDDIR) rm -rf /etc/pacman.d/gnupg/{openpgp-revocs.d/*,private-keys-v1.d/*}


	ln -fs /usr/lib/os-release $(BUILDDIR)/etc/os-release

	# add system users
	fakechroot -- fakeroot -- chroot $(BUILDDIR) /usr/bin/obsysusers

	# remove passwordless login for root (see CVE-2019-5021 for reference)
	sed -i -e 's/^root::/root:!:/' "$(BUILDDIR)/etc/shadow"

	# fakeroot to map the gid/uid of the builder process to root
	# fixes #22
	fakeroot -- tar --numeric-owner --xattrs --acls --exclude-from=exclude -C $(BUILDDIR) -c . -f $(OUTPUTDIR)/$(1).tar

endef

define dockerfile
	sed -e "s|TEMPLATE_IMAGE|$(1).tar|" \
	Dockerfile.template > $(OUTPUTDIR)/Dockerfile.$(1)
endef

.PHONY: clean
clean:
	rm -rf $(BUILDDIR) $(OUTPUTDIR)

$(OUTPUTDIR)/$(DOCKER_IMAGE).tar.xz:
	$(call rootfs,$(DOCKER_IMAGE))

$(OUTPUTDIR)/Dockerfile.$(DOCKER_IMAGE): $(OUTPUTDIR)/$(DOCKER_IMAGE).tar.xz
	$(call dockerfile,$(DOCKER_IMAGE))


.PHONY: docker-image
image: $(OUTPUTDIR)/Dockerfile.$(DOCKER_IMAGE)
	docker build -f $(OUTPUTDIR)/Dockerfile.$(DOCKER_IMAGE) -t $(DOCKER_ORGANIZATION)/$(DOCKER_IMAGE) $(OUTPUTDIR)

.PHONY: docker-image-push
push:
	docker login -u $(DOCKER_USER)
	docker push $(DOCKER_ORGANIZATION)/$(DOCKER_IMAGE)
